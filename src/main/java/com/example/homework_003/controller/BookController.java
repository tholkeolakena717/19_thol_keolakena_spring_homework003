package com.example.homework_003.controller;

import com.example.homework_003.Services.BookServices;
import com.example.homework_003.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class BookController {
    private final BookServices bookServices;

    public BookController(BookServices bookServices) {
        this.bookServices = bookServices;
    }

    @GetMapping("/api/v1/book")
    public ResponseEntity<?> getAllBook(){
        List<Books> books = bookServices.getAllBook();
        return ResponseEntity.ok( new ApiRepon<List<Books>>(
                LocalDateTime.now(),
                200,
                "Seccessfully fetched data!",
                books
        ));




    }
    @GetMapping("/api/v1/book/{id}")
    public ResponseEntity<?> getBookByid(@PathVariable Integer id){
        Books books = bookServices.getBookByid(id);
        return ResponseEntity.ok( new ApiRepon<Books>(
                LocalDateTime.now(),
                200,
                "Seccessfully fetched data!",
                books
        ));




    }
    @PostMapping("/api/v1/book/")
    public ResponseEntity<?> addBook(@RequestBody BookRequest bookRequest){
        Books books=bookServices.addBook(bookRequest);
        ApiRepon<Books> respon=ApiRepon.<Books>builder()
                .timeState(LocalDateTime.now())
                .message("Seccessfully fetched data!")
                .payload(books)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }

}
