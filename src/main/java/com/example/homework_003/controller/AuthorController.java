package com.example.homework_003.controller;

import com.example.homework_003.Services.AuthorServices;
import com.example.homework_003.model.ApiRepon;
import com.example.homework_003.model.Author;
import com.example.homework_003.model.AuthorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class AuthorController {
    private final AuthorServices authorServices;

    public AuthorController(AuthorServices authorServices) {
        this.authorServices = authorServices;
    }
     @GetMapping("/api/v1/authors")
    public ResponseEntity<?> getAllAuthor(){
        List<Author> author = authorServices.getAllAuthor();
        return ResponseEntity.ok( new ApiRepon<>(
                LocalDateTime.now(),
                200,
                "Seccessfully fetched data!",
                  author
                        ));




     }
     @GetMapping(" /api/v1/authors/{id} ")
     public ResponseEntity<?> getAuthorById(@PathVariable Integer id){
        Author author=authorServices.getAuthorById(id);
        ApiRepon<Author> respon=ApiRepon.<Author>builder()

                .timeState(LocalDateTime.now())
                .message("Seccessfully fetched data!")
                .payload(author)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

     }
    @PostMapping("/api/v1/author/")
    public ResponseEntity<?> addAuthor(@RequestBody AuthorRequest authorRequest){
        Author authors=authorServices.addAuthor(authorRequest);
        ApiRepon<Author> respon=ApiRepon.<Author>builder()
                .timeState(LocalDateTime.now())
                .message("Add Seccessfully fetched data!")
                .payload(authors)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }
    @PutMapping("/api/v1/author/{id}")
    public ResponseEntity<?> UpdateAuthor(@PathVariable Integer id, @RequestBody AuthorRequest authorRequest){
        Author authors=authorServices.UpdateAuthor(id,authorRequest);
        ApiRepon<Author> respon=ApiRepon.<Author>builder()
                .timeState(LocalDateTime.now())
                .message("Update Seccessfully fetched data!")
                .payload(authors)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);
    }
    @DeleteMapping(" /api/v1/author/{id}")
    public ResponseEntity<?> DeleteAuthorById(@PathVariable Integer id){
        Author author=authorServices.DeleteAuthorById(id);
        ApiRepon<Author> respon=ApiRepon.<Author>builder()

                .timeState(LocalDateTime.now())
                .message("Delete Seccessfully fetched data!")
                .payload(author)
                .status(200)
                .build();
        return ResponseEntity.ok(respon);

    }

}
