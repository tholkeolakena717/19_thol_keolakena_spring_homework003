package com.example.homework_003.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Author {

//    @JsonProperty(access =JsonProperty.Access.READ_ONLY)
    private Integer authorId;
    private String authorName;
    private String authorGender;
}
