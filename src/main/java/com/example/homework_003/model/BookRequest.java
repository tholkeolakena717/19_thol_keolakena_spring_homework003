package com.example.homework_003.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class BookRequest {
    private String title;
    private Timestamp pushlisedDate;
    private Integer authorId;
    private List<Integer> category;
}
