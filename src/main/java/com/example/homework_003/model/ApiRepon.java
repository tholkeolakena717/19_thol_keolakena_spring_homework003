package com.example.homework_003.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class ApiRepon<T> {

    private LocalDateTime timeState;
     private Integer status;
     private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
     private T payload;

}
