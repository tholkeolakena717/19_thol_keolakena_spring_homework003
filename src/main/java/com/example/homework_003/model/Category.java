package com.example.homework_003.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    @JsonProperty(access =JsonProperty.Access.READ_ONLY)
    private int categoryId;
    private String categoryName;

}
