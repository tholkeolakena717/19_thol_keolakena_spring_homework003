package com.example.homework_003.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Books {
   @JsonProperty(access = JsonProperty.Access.READ_ONLY)
   //    @JsonProperty(access =JsonProperty.Access.READ_ONLY)
    private int bookId;
    private String title;
    private Timestamp pushlisedDate;
    private Author author;
    private List<Category> category;
}
