package com.example.homework_003.Services;

import com.example.homework_003.Exception.BlanckFildException;
import com.example.homework_003.Exception.UserNotFoundException;
import com.example.homework_003.model.Author;
import com.example.homework_003.model.AuthorRequest;
import com.example.homework_003.reponsitory.AuthorReponsitory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.prefs.BackingStoreException;

@Service
public class AuthorServiceImplement implements   AuthorServices{
    private  final AuthorReponsitory  authorReponsitory;

    public AuthorServiceImplement(AuthorReponsitory authorReponsitory) {
        this.authorReponsitory = authorReponsitory;
    }

    @Override
    public List<Author> getAllAuthor() {
        return authorReponsitory.getAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer id) {
        Author author=authorReponsitory.getAuthorById(id) ;
        if(author==null){
            throw  new UserNotFoundException("User with id"+id+"not found");

        }
        return authorReponsitory.getAuthorById(id);
    }

    @Override
    public Author addAuthor(AuthorRequest authorRequest) {
        if(authorRequest.getAuthor_name().isBlank()){
            throw new BlanckFildException("Feild name is empty");
        }
        if(authorRequest.getGender().isBlank()){
            throw new BlanckFildException("Feild gender is empty");
        }

        return authorReponsitory.addAuthor(authorRequest);
    }

    @Override
    public Author UpdateAuthor(Integer id, AuthorRequest authorRequest) {
        Author author=authorReponsitory.getAuthorById(id);
            if (author == null) {
                throw new UserNotFoundException("User with id" + id + " not found ");

            }
        if(authorRequest.getAuthor_name().isBlank()){
            throw new BlanckFildException(" Feild name is empty");
        }
        if(authorRequest.getGender().isBlank()){
            throw new BlanckFildException(" Feild gender is empty");
        }
        authorReponsitory.UpdateAuthor(id, authorRequest);
        return  authorReponsitory. getAuthorById(id);
    }

    @Override
    public Author DeleteAuthorById(Integer id) {
        Author author=authorReponsitory.getAuthorById(id);
        if (author == null) {
            throw new UserNotFoundException("User with id" + id + "not found");

        }
         authorReponsitory.DeleteAuthorById(id);
        return  authorReponsitory. getAuthorById(id);
    }
}
