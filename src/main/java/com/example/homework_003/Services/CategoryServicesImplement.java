package com.example.homework_003.Services;

import com.example.homework_003.Exception.BlanckFildException;
import com.example.homework_003.Exception.UserNotFoundException;
import com.example.homework_003.model.Books;
import com.example.homework_003.model.Category;
import com.example.homework_003.model.CategoryRequest;
import com.example.homework_003.reponsitory.CategoryResponsitory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServicesImplement   implements   CategoryServices {
    private final CategoryResponsitory  categoryResponsitory;

    public CategoryServicesImplement(CategoryResponsitory categoryResponsitory) {
        this.categoryResponsitory = categoryResponsitory;
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryResponsitory.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer id) {
        Category category=categoryResponsitory.getCategoryById(id);
        if (category == null) {
            throw new UserNotFoundException("User with id" + id + "not found");
        }
        return categoryResponsitory.getCategoryById(id);
    }

    @Override
    public Category addCategory(CategoryRequest categoryRequest) {
        if(categoryRequest.getCategoryName().isBlank()){
            throw new BlanckFildException("Feild name is empty");
        }

        return categoryResponsitory.addCategory(categoryRequest);
    }

    @Override
    public Category updateCategory(Integer id, CategoryRequest categoryRequest) {
        categoryResponsitory.updateCategory(id,categoryRequest);
        Category category=categoryResponsitory.getCategoryById(id);
        if (category == null) {
            throw new UserNotFoundException("User with id" + id + "not found");
        }
        if(categoryRequest.getCategoryName().isBlank()){
            throw new BlanckFildException("Feild name is empty");
        }
        return categoryResponsitory.getCategoryById(id);
    }

    @Override
    public Category deleteCategory(Integer id) {
        Category category=categoryResponsitory.getCategoryById(id);
        if (category == null) {
            throw new UserNotFoundException(" User with id" + id + " not found");
        }
        return categoryResponsitory.deleteCategory(id);
    }


}
