package com.example.homework_003.Services;

import com.example.homework_003.Exception.BlanckFildException;
import com.example.homework_003.Exception.UserNotFoundException;
import com.example.homework_003.model.Author;
import com.example.homework_003.model.BookRequest;
import com.example.homework_003.model.Books;
import com.example.homework_003.reponsitory.BookRespository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplement implements  BookServices{

    private final BookRespository  bookRespository;

    public BookServiceImplement(BookRespository bookRespository) {
        this.bookRespository = bookRespository;
    }

    @Override
    public List<Books> getAllBook() {
        return bookRespository.getAllBook();
    }

    @Override
    public Books getBookByid(Integer id) {
        Books books=bookRespository.getBookByid(id);
        if (books == null) {
            throw new UserNotFoundException("User with id" + id + "not found");
        }
        return bookRespository.getBookByid(id);
    }

    @Override
    public Books addBook(BookRequest bookRequest) {
        Integer book_id=bookRespository.addBook(bookRequest);
        for(Integer id:bookRequest.getCategory()){
            bookRespository.insertCategoryDatils(book_id,id);
        }
        if(bookRequest.getTitle().isBlank()){
            throw new BlanckFildException("Feild name is empty");
        }



        return bookRespository.getBookByid(book_id);
    }



}
