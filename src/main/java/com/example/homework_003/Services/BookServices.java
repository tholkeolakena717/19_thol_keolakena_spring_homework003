package com.example.homework_003.Services;

import com.example.homework_003.model.BookRequest;
import com.example.homework_003.model.Books;

import java.util.List;

public interface BookServices {
    List<Books> getAllBook();

    Books getBookByid(Integer id);



    Books addBook(BookRequest bookRequest);
}
