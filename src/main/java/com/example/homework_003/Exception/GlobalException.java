package com.example.homework_003.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.URI;
import java.time.LocalDateTime;


@RestControllerAdvice
public class GlobalException {
    @ExceptionHandler(UserNotFoundException.class)
    ProblemDetail handelUserNotFoundException(UserNotFoundException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setType(URI.create("localhost:8096/error"));
        problemDetail.setProperty("time", LocalDateTime.now());


        return problemDetail;
    }
    @ExceptionHandler(BlanckFildException.class)
    ProblemDetail handelUserNotFoundException(BlanckFildException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());


        return problemDetail;
    }

}
