package com.example.homework_003.reponsitory;

import com.example.homework_003.model.Author;
import com.example.homework_003.model.AuthorRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Mapper
public interface AuthorReponsitory {
    @Select("""
            select * from authors
            """)
    @Results(id = "mapResult", value={
            @Result(property = "authorId",column = "author_id"),
            @Result(property = "authorName",column = "author_name"),
            @Result(property = "authorGender",column = "gender")
    })

    List<Author> getAllAuthor();
    @Select("""
            select * from authors where author_id =#{id}
            """)
    @ResultMap("mapResult")
    @Result(property = "author_id",column = "author_id")
    Author getAuthorById(Integer id);
    @Select("""
            insert into authors(author_name, gender) VALUES (#{author_name},#{gender})
            RETURNING *
            """)
    @ResultMap("mapResult")
    Author addAuthor(AuthorRequest authorRequest);
      @Update("""
              update authors
              set author_name=#{author.author_name},gender=#{author.gender}
              where author_id=#{id};
              """)
      @ResultMap("mapResult")

    void UpdateAuthor(@PathVariable("id") Integer id, @Param("author") AuthorRequest authorRequest);
    @Delete("""
              delete from authors where author_id=#{id};
              """)
    @ResultMap("mapResult")


    void DeleteAuthorById(Integer id);
}
