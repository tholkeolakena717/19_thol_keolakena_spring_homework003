package com.example.homework_003.reponsitory;

import com.example.homework_003.model.Category;
import com.example.homework_003.model.CategoryRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface CategoryResponsitory {
    @Select("select c.category_id,c.category_name\n" +
            "from categories c INNER JOIN book_details bd on c.category_id = bd.category_id\n" +
            "where book_id=#{id}")
    @ResultMap("categoryMap")
    List<Category> getAllCategoryByBookId (Integer id);

    @Select("""
            select * from categories;
            """)
    @Results(id = "categoryMap", value ={
            @Result(property="categoryName",column ="category_name"),
            @Result(property ="categoryId",column ="category_id")
    })
    List<Category> getAllCategory();
    @Select("""
            select * from categories where category_id=#{id};
            """)
    @ResultMap("categoryMap")

    Category getCategoryById(Integer id);
    @Select("""
            insert into categories(category_name) VALUES (#{category.categoryName})
            RETURNING *
            """)
    @ResultMap("categoryMap")
    Category addCategory(@Param("category") CategoryRequest categoryRequest);
    @Update("""
              update categories
              set category_name=#{category.categoryName}
              where category_id=#{id};
              """)
    @ResultMap("categoryMap")
    void updateCategory(@PathVariable("id") Integer id,@Param("category") CategoryRequest categoryRequest);
      @Delete("""
              delete from categories  where category_id=#{id}
              """)
    Category deleteCategory(@Param("id") Integer id);
}
