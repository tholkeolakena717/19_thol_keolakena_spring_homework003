package com.example.homework_003.reponsitory;

import com.example.homework_003.model.BookRequest;
import com.example.homework_003.model.Books;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface BookRespository {
//    @Results(id = "MapBook",value = {
//            @Result(property="bookId",column ="book_id"),
//            @Result(property="title",column ="title"),
//            @Result(property="pushlisedDate",column ="published_date"),
//
//
//
//
//    })


    @Select("""
            select * from books
      """)
    @Result(property="pushlisedDate",column ="published_date")
    @Result(property="bookId",column ="book_id")
    @Result(property="author",column ="author_id",
            one =@One(select = "com.example.homework_003.reponsitory.AuthorReponsitory.getAuthorById"))
    @Result(property = "category",column = "book_id",many = @Many(select = "com.example.homework_003.reponsitory.CategoryResponsitory.getAllCategory"))
    





    List<Books> getAllBook();
    @Select("""
            select * from books where book_id=#{id}
      """)
    @Result(property="pushlisedDate",column ="published_date")
    @Result(property="bookId",column ="book_id")
    @Result(property="author",column ="author_id",
            one =@One(select = "com.example.homework_003.reponsitory.AuthorReponsitory.getAuthorById"))
    @Result(property = "category",column = "book_id",many = @Many(select = "com.example.homework_003.reponsitory.CategoryResponsitory.getAllCategoryByBookId"))
    Books getBookByid(Integer id);

      @Select("""
              insert into books(title, published_date, author_id) 
              VALUES (#{book.title},#{book.pushlisedDate},#{book.authorId})
              RETURNING book_id 
              """)
    Integer addBook(@Param("book") BookRequest bookRequest);
      @Insert("insert into book_details(category_id, book_id) VALUES (#{id},#{bookId})")


    void insertCategoryDatils(Integer bookId, Integer id);
}
